FROM node:7
RUN mkdir /todobackend
ADD . /todobackend
WORKDIR /todobackend
RUN npm i
EXPOSE 80
CMD ["npm", "start"]